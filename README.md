# Repaso de Javascript

## Elementos básicos del lenguaje

### Variables

En JS las variables no tienen asignado un tipo, a diferencia de lenguajes
como C# o Java, pero aún así es necesario declararlas para identificar su
ámbito.

En JS moderno existen dos formas de declarar una variable:

```js
const foo = "Example"
let bar = "Change me"
```

Cuando usamos `const`, la variable no puede cambiar de valor nunca. Cuando
usamos `let`, podemos cambiarle el valor de forma normal. Si podemos usar
`const`, siempre es mejor, pero la mayoría de veces necesitaremos `let`.

Tradicionalmente se usaba `var`, pero hoy en día no se debe usar **nunca**.

### Operadores

Son principalmente los mismos que en C# o Java:

```js
const nombre = 'Pepe'
const saludo = 'Hola, '
const frase = saludo + nombre

const a = 3 + 2
const b = a - 1
const c = a * b

const modulo = 25 % 10 // = 5, el resto de la división de 25 entre 10
const booleanos = true && (false || true)
const comparacion = c === 20 // en general usaremos el TRIPLE igual (===), no el DOBLE (==)
```

### Funciones

Históricamente se creaban así:

```js
function miFuncion(param1, param2) {
    // ...
    return 123
}
```

Hoy en día existe una forma más breve llamada *arrow functions*:

```js
const miFuncion = (param1, param2) => {
    // ...
    return 123
}
```

Una ventaja importante de las arrow function es que si la función es algo muy
corto, podemos omitir las llaves y el return, que será implícito:

```js
const sumar = (a, b) => a + b

// Equivale a: (a, b) => { return a + b }
```

Podemos mezclar los dos tipos de funciones sin que esto genere ningún problema.

### Bucles

Son exactamente igual que en otros lenguajes como C# o Java:

```js
for (let i = 0; i < 10; i++) {
    // ...
}

while (n > 0) {
    // ...
}
```

Más adelante al trabajar con arrays surgen otras formas de recorrer de forma _funcional_, que veremos más adelante.

### Condicionales

En JS tenemos el condicional clásico de otros lenguajes:

```js
if (n > 100) {
    // ...
} else if (n > 50) {
    // ...
} else {
    // ...
}
```

Pero también tenemos un operador _ternario_ que nos puede servir para evitar un if completo:

```js
const a = n > 100 ? 'mayor' : 'menor'
// a será 'mayor' si n es mayor que 100, o 'menor' en caso contrario
```

### Arrays

Cuando queremos tener una lista de elementos, debemos utilizar arrays. Funcionan así:

```js
const l = [1, 2, 3]

l.push(4) // añade el elemento 4 al final de la lista

const n = l[0] // n = 1, el valor en la posición 0 de la lista

l[1] = 100 // ahora l = [1, 100, 3, 4]
```

Existen muchas funciones con listas que se pueden consultar en
[MDN](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Array),
pero hay dos especialmente importantes que tenemos que conocer para este curso:
filter y map.

Filter se utiliza para crear una **nueva lista** con los elementos de otra que
cumplen cierta condición, que indicamos mediante una función:

```js
const lista = [1, 2, 3, 4, 5]

const esPar = (n) => n % 2 === 0 // Definimos una función que nos indica si un nº es par
const pares = lista.filter(esPar) // Creamos una lista con sólo los elementos pares

// Habitualmente se haría en un sólo paso, definiendo la función en el propio parámetro:
const pares2 = lista.filter(n => n % 2 === 0)
```

Map se utiliza para crear una **nueva lista** con los resultados de aplicar una
función a cada elemento de la lista original:

```js
const lista = [1, 2, 3, 4, 5]

const doble = (n) => 2 * n
const dobles = lista.map(doble) // = [2, 4, 6, 8, 10]

// O en un solo paso:
const dobles2 = lista.map(n => 2 * n)
```

### Objetos

Sin entrar en programación orientada a objetos, en JS se utilizan mucho los
objetos como una forma de mantener datos relacionados juntos en un sólo sitio.

Se usan así:

```js
const paco = { nombre: 'Francisco', edad: 75, pais: 'España' }

// Para acceder a los campos:
const p = paco['pais']
const p2 = paco.pais

// El segundo método es el recomendado, por ser más simple, pero no sirve si el campo es una variable:
const campo = 'nombre'
const name = paco[campo] // = paco['nombre'] = 'Francisco'

// Se pueden añadir nuevos campos, o cambiar los existentes, de ambas formas:
paco['ciudad'] = 'Lugo'
paco.ciudad = 'Lugo'
```

### Destructuring

En versiones modernas de JS, existe una nueva sintaxis que permite extraer
variables de estructuras (como arrays o objetos) de una forma más sencilla.
Esto se utiliza muchísimo en código moderno, por lo que es importante
entenderlo a la perfección.

```js
const persona = { nombre: 'Francisco', edad: 75, pais: 'España' }

// Antes haríamos algo como:
const nombre = persona.nombre
const edad = persona.edad

// Ahora podemos hacerlo en un solo paso:
const { nombre, edad } = persona

// También aplica con arrays:
const lista = ['Coruña', 'Lugo', 'Ourense', 'Pontevedra']
const [ c1, c2 ] = lista // c1 = 'Coruña', c2 = 'Lugo'

// En ambos casos se puede utilizar también '...' para indicar "todos los demás":
const [ primera, ...resto ] = lista // primera = 'Coruña', resto = ['Lugo', 'Ourense', 'Pontevedra']
const { nombre, ...resto } = persona // nombre = 'Francisco', resto = { edad: 75, pais: 'España' }

// Cuando se trata de objetos, se puede utilizar un nombre diferente para la variable y el campo usando ":" como separador:
const { nombre: name } = persona // name = persona.nombre = 'Francisco'; OJO: la variable aquí se llama name, y contiene el campo nombre.

// Todo esto se puede utilizar en cualquier sitio, no solo al declarar variables.
// Es muy común hacerlo, por ejemplo, al definir una función:
function saludar({ nombre, pais }) { // <-- Ojo a las llaves!!
    // Esta función recibe un OBJETO, y se queda con el campo nombre y pais en variables del mismo nombre
    console.log('Hola,' + nombre, ', de', pais)
}
saludar(persona)
```
